package com.example.kotlin_sqlite

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail_information.*


class DetailInformation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_information)

        var dtitle = intent.getStringExtra("title")


        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = dtitle

        Detail_txtTitle.text = dtitle
        val byteArray:ByteArray = intent.getByteArrayExtra("image")
        val bmp: Bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
        Glide.with(applicationContext)
            .load(bmp)
            .error(R.drawable.noimage)
            .into(Detail_imageView)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> finish()
        }
        return true
    }
}
