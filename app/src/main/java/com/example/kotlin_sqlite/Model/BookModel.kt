package com.example.sqlite.Model

class BookModel {

    var code:Int =  0
    var title:String = ""
    var author:String = ""
    var price:Double = 0.0
    var photo:ByteArray? =null

    constructor()

    constructor(code: Int, title: String, author: String, price: Double, photo: ByteArray) {
        this.code = code
        this.title = title
        this.author = author
        this.price = price
        this.photo = photo
    }

    override fun toString(): String {
        return "$code$title$author$price$photo"
    }
}