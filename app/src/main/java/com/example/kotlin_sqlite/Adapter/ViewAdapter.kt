package com.example.kotlin_sqlite.Adapter

import android.content.Context
import android.gesture.GestureLibraries
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kotlin_sqlite.R
import com.example.kotlin_sqlite.ViewInformation
import com.example.sqlite.Model.BookModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_view_infomation.view.*
import java.io.ByteArrayOutputStream
import java.text.DecimalFormat

class ViewAdapter(var context: Context, var lists :ArrayList<BookModel>) : RecyclerView.Adapter<ViewAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view =LayoutInflater.from(context).inflate(R.layout.custom_view_infomation,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var bookModel = lists[position]
        holder.setData(bookModel,context)
    }

    inner class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        var context :Context? =null
        init {
            itemView.setOnClickListener{
                Toast.makeText(context,"${lists.size}",Toast.LENGTH_SHORT).show()
                if(context is ViewInformation){
                    var viewInformation:ViewInformation = context as ViewInformation
                    viewInformation.getAdapterPosition(adapterPosition)
                }
            }
        }

        fun setData(bookModel: BookModel,context: Context) {
            itemView.View_txtTitle.text = bookModel.title
            itemView.View_txtAuthor.text = bookModel.author

            itemView.View_txtPrice.text = DecimalFormat("0.00 $").format(bookModel.price).toString()

            var byteArray:ByteArray? = bookModel.photo
            var bitmap:Bitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray!!.size)
            //itemView.view_imageView.setImageBitmap(bitmap)

//            //normal glide
//            Glide.with(context)
//                .load(bitmap)
//                .into(itemView.view_imageView)

            //convert image from bitmap to small
            Glide.with(context)
                .load(byteArray)
                .error(R.drawable.noimage)
                .override(100, 200)
                .fitCenter()
                .into(itemView.view_imageView)

            this.context = context
        }

    }

}