package com.example.kotlin_sqlite

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.widget.EditText
import android.widget.Toast
import com.example.kotlin_sqlite.Adapter.BottomSheetDialogAdapter
import com.example.kotlin_sqlite.Adapter.ViewAdapter
import com.example.sqlite.DBHelper
import com.example.sqlite.Model.BookModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream
import java.io.InputStream

class MainActivity : AppCompatActivity(){

    companion object{
        //image pick code
        private val IMAGE_PICK_CODE = 1000
        //Permission code
        private val PERMISSION_CODE = 1001
        //store path image
        lateinit var path: Uri

        lateinit var byteImage:ByteArray
    }
    var c:Int=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        var dbHelper = DBHelper(this)
        c = dbHelper.getMaxCode("select code from tbBook").toString().toInt() + 1
        Toast.makeText(applicationContext,c.toString(),Toast.LENGTH_SHORT).show()
        txtCode.setText(c.toString())
        txtCode.gravity = Gravity.CENTER
        txtTitle.requestFocus()


        btnBrowse.setOnClickListener{
            //check runtime permission
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                    //permission deny
                    var permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                    //show pop up request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE)
                }
                else{
                    //permission already granted
                    pickImageFromGallery()
                }
            }
            else{
                //system OS < Marshamllow
                pickImageFromGallery()
            }
        }
        btnAdd.setOnClickListener{

            if(TextUtils.isEmpty(txtTitle.text))
                validateControl(txtTitle,"Please type Title")
            else if(TextUtils.isEmpty(txtAuthor.text))
                validateControl(txtAuthor,"Please type Author")
            else if(TextUtils.isEmpty(txtPrice.text))
                validateControl(txtPrice,"Please type Price")
            else if(imageView.drawable == null){
                Toast.makeText(applicationContext,"Please Choose Photo",Toast.LENGTH_SHORT).show()
            }
            else{
                var code:Int = txtCode.text.toString().toInt()
                var title:String  = txtTitle.text.toString()
                var author:String  = txtAuthor.text.toString()
                var price:Double  = txtPrice.text.toString().toDouble()
                var bookModel = BookModel(code,title,author,price,byteImage)
                dbHelper.bookInsert(bookModel)
                txtCode.setText((code+1).toString())
                clearControl()
            }
        }
        btnView.setOnClickListener{
            startActivity(Intent(applicationContext,ViewInformation::class.java))
        }
        btnEdit.setOnClickListener {
            var bottomSheetDialogAdapter = BottomSheetDialogAdapter()
            bottomSheetDialogAdapter.show(supportFragmentManager,"adapter")
        }
    }

    //pick image from gallery by click button
    private fun pickImageFromGallery() {
        var intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    //handle request permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){
            PERMISSION_CODE -> {
                if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            imageView.setImageURI(data?.data)
            path = data?.data!!
            var inputStream: InputStream? = contentResolver.openInputStream(path)
            val byteBuffer = ByteArrayOutputStream()
            val bufferSize = 1024
            val buffer = ByteArray(bufferSize)
            var len:Int = 0
            while (inputStream!!.read(buffer).also { len = it } != -1) {
                byteBuffer.write(buffer, 0, len)
            }
            byteImage = byteBuffer.toByteArray()


        }

    }

    //clear control
    private fun clearControl(){
        txtTitle.text = null
        txtAuthor.text = null
        txtPrice.text  = null
        imageView.setImageResource(R.drawable.cus_image)
        txtTitle.requestFocus()
    }

   //check emptry string
    fun validateControl(txt:EditText,text:String){
       txt.error = text
       txt.text = null
       txt.requestFocus()

   }

    override fun onBackPressed() {
        var alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Closing")
        alertDialog.setMessage("Do you want to close ? Please Confirms me To Know")
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton("yes", DialogInterface.OnClickListener { dialog, which -> finish() })
        alertDialog.setNegativeButton("no", DialogInterface.OnClickListener { dialog, which ->  })

        var builde = alertDialog.create()
        builde.show()
    }
}
